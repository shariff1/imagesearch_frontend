// Observable Version
import {Component, OnInit} from '@angular/core';
import {Hero} from './capital';
import {HeroService} from '../service/bundesland.service';
import {ImageHelper} from "./helper";

@Component({
    selector: 'hero-list',
    templateUrl: '/app/src/html/bundesland.component.html',
    providers: [HeroService],
    styles: ['.error {color:red;}'],
})

export class BundesLandListComponent implements OnInit {
    errorMessage: string;
    heroes: Hero[];
    model = new ImageHelper('');

    constructor(private heroService: HeroService) {
    }

    ngOnInit() {
        this.getCapitals();
    }

    getCapitals() {
        this.heroService.getBundesland(this.model.searchStr)
            .subscribe(
                heroes => this.heroes = heroes,
                error => this.errorMessage = <any>error);
    }


}


