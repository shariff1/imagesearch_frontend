import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';

import {requestOptionsProvider} from './default-request-options.service';

import {AppComponent} from './app.component';

import {BundesLandListComponent} from '../resource/bundesland-list';


@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        JsonpModule,

    ],
    declarations: [
        AppComponent,
        BundesLandListComponent

    ],
    providers: [requestOptionsProvider],
    bootstrap: [AppComponent]
})
export class AppModule {
}



